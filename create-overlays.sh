#!/bin/bash -xe
PREFIX=/home/ablu/projects/trs/build/tmp_trs-qemuarm64/deploy/images/trs-qemuarm64

cp --reflink=always "$PREFIX/trs-image-trs-qemuarm64.rootfs.wic" ./
cp --reflink=always "$PREFIX/Image" ./

# create overlays:
qemu-img create -f qcow2 -b trs-image-trs-qemuarm64.rootfs.wic -F raw trs-overlay-host.qcow2
qemu-img create -f qcow2 -b trs-image-trs-qemuarm64.rootfs.wic -F raw trs-overlay-guest.qcow2
