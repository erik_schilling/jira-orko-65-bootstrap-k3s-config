#!/bin/bash 

while [[ ! -e "${LOG_DIR}/fs.sock0" ]]
do
    sleep 0.1
done

echo "socket available!"
set -xe

# sudo chown ablu:ablu "${LOG_DIR}/fs.sock0"

# ~/projects/qemu/build/qemu-system-aarch64 -kernel Image -machine virt \
#     -cpu max,pauth-impdef=on -serial mon:stdio -device virtio-net-pci,netdev=net0 -netdev user,id=net0,hostfwd=tcp::2223-:22 \
#     -display none -m 2048 -smp 4 \
#     -object memory-backend-memfd,id=mem,size=2048M,share=on -numa node,memdev=mem \
#     -chardev socket,id=char0,path="${LOG_DIR}/fs.sock0" \
#     -device vhost-user-fs-pci,queue-size=1024,chardev=char0,tag=root \
#     -append 'rootfstype=virtiofs root=root ro'

~/projects/qemu/build/qemu-system-aarch64 -kernel Image -machine virt \
    -cpu cortex-a57 -serial mon:stdio -device virtio-net-pci,netdev=net0 -netdev user,id=net0,hostfwd=tcp::2223-:22 \
    -display none -m 2048 -smp 4 \
    -object memory-backend-memfd,id=mem,size=2048M,share=on -numa node,memdev=mem \
    -hda trs-overlay-guest.qcow2 \
    -chardev socket,id=char0,path="${LOG_DIR}/fs.sock0" \
    -device vhost-user-fs-pci,queue-size=1024,chardev=char0,tag=/dev/root \
    -append 'root=/dev/vda2 ro log_buf_len=8M'
