#!/bin/bash 

set -xe

# sudo chown ablu:ablu "${LOG_DIR}/fs.sock0"

# ~/projects/qemu/build/qemu-system-aarch64 -kernel Image -machine virt \
#     -cpu max,pauth-impdef=on -serial mon:stdio -device virtio-net-pci,netdev=net0 -netdev user,id=net0,hostfwd=tcp::2223-:22 \
#     -display none -m 2048 -smp 4 \
#     -object memory-backend-memfd,id=mem,size=2048M,share=on -numa node,memdev=mem \
#     -chardev socket,id=char0,path="${LOG_DIR}/fs.sock0" \
#     -device vhost-user-fs-pci,queue-size=1024,chardev=char0,tag=root \
#     -append 'rootfstype=virtiofs root=root ro'

~/projects/qemu/build/qemu-system-x86_64 -machine pc \
    -cpu max -serial mon:stdio \
    -device virtio-net-pci,netdev=net0 -netdev user,id=net0,hostfwd=tcp::2223-:22 \
    -display none -m 2048 -smp 1 \
    -object memory-backend-memfd,id=mem,size=2048M,share=on,prealloc=on,hugetlb=off -numa node,memdev=mem \
    -hda fedora-x86.img \
    -chardev socket,id=char0,path="fs.sock0" \
    -device vhost-user-fs-pci,queue-size=1024,chardev=char0,tag=/dev/root 
