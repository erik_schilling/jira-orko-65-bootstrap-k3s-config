#!/usr/bin/env python3
from pwn import *

context.terminal = ["tmux", "splitw", "-h"]
os.environ['DEBUGINFOD_URLS'] = 'https://debuginfod.fedoraproject.org/'

virtiofsd = process([
    'cargo', 'run', '--',
    '--seccomp=none', '--sandbox=none',
    '--socket-path', '/home/ablu/projects/jira/orko-65-bootstrap-k3s-config/x86_test/fs.sock0',
    '--shared-dir', '/home/ablu/projects/jira/orko-65-bootstrap-k3s-config/x86_test/shared-dir/'
],
    env={'RUST_LOG': "debug"},
    cwd="/home/ablu/projects/rust-vmm/virtiofsd")
virtiofsd.recvline_contains(b'Waiting for vhost-user socket connection')

qemu = process([
    '/home/ablu/projects/qemu/build/qemu-system-x86_64',
    # '-machine', 'pc',
    #             '-cpu', 'max',
                '-serial', 'mon:stdio',
               # '-serial', 'file:log.txt',
               '-kernel', 'bzImage',
                '-s', '-S',
                '-device', 'virtio-net-pci,netdev=net0',
                '-netdev', 'user,id=net0,hostfwd=tcp::2223-:22',
                '-display', 'none',
                '-m', '2048',
                '-smp', '1',
                '-object', 'memory-backend-memfd,id=mem,size=2048M,share=on,prealloc=on,hugetlb=off',
                '-numa', 'node,memdev=mem',
               '-initrd', 'initramfs-6.2.15-300.fc38.x86_64.img',
                '-hda', 'fedora-x86.img',
               '-append', 'root=/dev/sda3 console=ttyS0 nokaslr',
                '-chardev', 'socket,id=char0,path=fs.sock0',
                '-device', 'vhost-user-fs-pci,queue-size=1024,chardev=char0,tag=/dev/root'
])
# qemu.interactive()

sleep(1)
(_, dbg) = gdb.attach(
    ('0.0.0.0', 1234),
    exe="vmlinux",
    api=True)
dbg.execute('set debuginfod enabled on')
dbg.execute('set debuginfod urls https://debuginfod.fedoraproject.org/')
dbg.execute('file vmlinux')
dbg.execute('source /home/ablu/projects/linux-v6.6/vmlinux-gdb.py')
dbg.continue_nowait()

elf = ELF('vmlinux')

def ssh(cmd):
    ssh = process(['ssh', '-o', 'LogLevel=ERROR', 'qemu2', cmd])
    out = ssh.recvall()
    debug('ssh out: %s', out)
    return out.strip()

def interrupt():
    dbg.execute('interrupt')
    dbg.wait()

def cont():
    dbg.continue_nowait()


def dump(expr):
    info('value of: %s: %s', expr, dbg.execute(f'p {expr}', to_string=True))


class BreakpointAtRequest(dbg.Breakpoint):
    def __init__(self):
        super().__init__('virtio_fs_enqueue_req')

    def stop(self):
        info('virtio_fs_enqueue_req')
        dump('req->args->out_args')
        dump('req->args->in_args')
        return False


class BreakpointAtFuseAlloc(dbg.Breakpoint):
    def __init__(self):
        super().__init__('fs/fuse/dev.c:59')

    def stop(self):
        info('alloc req')
        dump('req')
        return False


class BreakpointAtFuseFree(dbg.Breakpoint):
    def __init__(self):
        super().__init__('fuse_request_free')

    def stop(self):
        info('free req')
        dump('req')
        return False


class BreakpointAtFuseWaitAnswer(dbg.Breakpoint):
    def __init__(self):
        super().__init__('request_wait_answer')

    def stop(self):
        info('await req')
        dump('$lx_current().pid')
        dump('req')
        return False

def get_ret_of(function):
    ret_thunk_addr = hex(elf.symbols['__x86_return_thunk'])
    code = elf.disasm(elf.symbols[function], 200)
    for line in code.splitlines():
        if ret_thunk_addr in line:
            # return offset of jmp to ret
            return int(line.split(':')[0], 16)
    assert False


class BreakpointAtFuseWaitAnswerDone(dbg.Breakpoint):
    def __init__(self):
        ret = get_ret_of('request_wait_answer')
        super().__init__(f'*{hex(ret)}')

    def stop(self):
        info('await req done')
        dump('req')
        return False


class BreakpointAtScatterlistInit(dbg.Breakpoint):
    def __init__(self):
        super().__init__('virtio_fs.c:1105')

    def stop(self):
        info('sg_init_fuse_args')
        dump('len')
        dump('ap->pages')
        dump('argpages')
        dump('sg[0]')
        dump('sg[1]')
        dump('sg[2]')
        dump('sg[3]')
        dump('sg[4]')
        dump('sg[5]')
        return False


class BreakpointAtScatterlistBuild(dbg.Breakpoint):
    def __init__(self):
        super().__init__('virtio_fs.c:1083')

    def stop(self):
        info('sg_init_fuse_pages')
        dump('i')
        dump('sg[i]')
        dump('pages[i]')
        dump('page_descs[i].offset')
        return False


class BreakpointAtRequestDone(dbg.Breakpoint):
    def __init__(self):
        super().__init__('virtio_fs_request_complete')

    def stop(self):
        info('virtio_fs_request_complete')
        dump('$lx_current().pid')
        dump('*req->args')
        return False


class BreakpointAtRequestDonePerPage(dbg.Breakpoint):
    def __init__(self):
        super().__init__('virtio_fs.c:579')

    def stop(self):
        info('virtio_fs_request_complete - loop')
        dump('i')
        dump('thislen')
        dump('len')
        dump('ap->pages[i]')
        dump('ap->descs[i]')
        dump('*ap')
        return False


def break_at_response():
    pass


def process_virtiofsd_log():
    while True:
        info('virtiofsd: %s', virtiofsd.recvline())
    

virtiofs_thread = Thread(target=process_virtiofsd_log)
virtiofs_thread.start()

p = log.progress('Bootstrap')
p.status('Waiting for boot')
qemu.recvuntil(b'fedora login:')
p.status('Logging in')
qemu.sendline(b'root')
qemu.recvuntil(b'Password:')
qemu.sendline(b'test')
qemu.recvuntil(b']#')

p.status('Loading modules into GDB')
modules = {}
# for module in ["virtiofs", "fuse"]:
#     modules[module] = {
#         'text': int(ssh(f'cat /sys/module/{module}/sections/.text'), 16),
#         'data': int(ssh(f'cat /sys/module/{module}/sections/.data'), 16),
#     }
if len(modules) > 0:
    interrupt()
    for name, config in modules.items():
        text = config['text']
        data = config['data']
        dbg.execute(f'add-symbol-file ./{name}.ko {hex(text)} -s .data {hex(data)}')
    cont()

p.status('mounting virtiofs')
ssh('mount -t virtiofs /dev/root /mnt/')

p.success('Bootstrap completed')

global test_run
test_run = 0

def test():
    global test_run
    test_run += 1
    p = log.progress('Test %s', test_run)
    p.status('setting up breakpoints')
    interrupt()
    bps = [
        BreakpointAtRequest(),
        BreakpointAtFuseAlloc(),
        BreakpointAtFuseFree(),
        BreakpointAtFuseWaitAnswer(),
        BreakpointAtFuseWaitAnswerDone(),
        # BreakpointAtScatterlistInit(),
        BreakpointAtScatterlistBuild(),
        BreakpointAtRequestDone(),
        BreakpointAtRequestDonePerPage(),
    ]
    cont()

    p.status('enabling TLB miss logging')
  
    p.status('running loader')
    result = ssh('/mnt/ld-linux-x86-64.so.2')
    info('Program out: %s', result)
    crashed = b'missing program name' not in result

    p.status('disabling TLB miss logging')
    p.status('disabling Breakpoints')
    interrupt()
    for bp in bps:
        bp.delete()
    cont()

    if crashed:
        info('Crashed!')
        p.success('Crashed!')
    else:
        info('Unable to reproduce...')
        p.failure('Failed to crash')

test()

import pdb
pdb.set_trace()
# [root@fedora ~]# cat /sys/module/virtiofs/sections/.text
# 0xffffffffc0525000
# [root@fedora ~]# cat /sys/module/virtiofs/sections/.data
# 0xffffffffc052b1c0
