#!/bin/bash -xe

echo test
until ssh qemu2 echo 1
do
	sleep 0.1
done

ssh qemu2 mount -t virtiofs /dev/root /mnt

# ssh qemu2 perf ftrace --graph-funcs='*fuse*' --graph-funcs='*virtio_fs*' --buffer-size=10M -a "bash -c '/mnt/ld-linux-aarch64.so.1'" > "$LOG_DIR/ftrace-loader.txt"

#ssh qemu2 /mnt/ld-linux-aarch64.so.1
