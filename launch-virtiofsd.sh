#!/bin/bash -xe

pushd ~/projects/rust-vmm/virtiofsd/
cargo build
popd

cp ~/projects/rust-vmm/virtiofsd/target/debug/virtiofsd ./
export RUST_LOG="debug"

RUST_LOG="debug" ./virtiofsd --seccomp=none --sandbox=none \
  --socket-path "${LOG_DIR}/fs.sock0" --shared-dir $PWD/share-dir/ --cache=never 2>&1  | tee "${LOG_DIR}/virtiofsd.txt"
# sudo ./virtiofsd --thread-pool-size=4 --seccomp=none --sandbox=none \
#   --socket-path "${LOG_DIR}/fs.sock0" --shared-dir /run/media/ablu/rootfs 
# RUST_LOG="debug" ./virtiofsd --seccomp=none --sandbox=none \
#   --socket-path "${LOG_DIR}/fs.sock0" --shared-dir /run/media/ablu/rootfs --cache=never 2>&1 | tee "${LOG_DIR}/virtiofsd.txt"

