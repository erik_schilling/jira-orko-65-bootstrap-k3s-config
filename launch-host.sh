#!/bin/bash -xe

qemu-system-aarch64 -kernel Image -machine virt \
    -cpu max,pauth-impdef=on -serial mon:stdio -device virtio-net-pci,netdev=net0 -netdev user,id=net0,hostfwd=tcp::2222-:22 \
    -hda trs-overlay-host.qcow2 -display none -m 2048 -smp 4 \
    -object memory-backend-memfd,id=mem,size=2048M,share=on -numa node,memdev=mem \
    -append 'root=/dev/vda2'

