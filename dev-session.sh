#!/bin/bash -xe

export LOG_DIR="$PWD/logs/$(date --rfc-3339=s)"
mkdir "$LOG_DIR"

session='orko-65-bootstrap-k3s-config'

tmux start-server
tmux new-session -d -s $session

tmux send-keys "./launch-host.sh; tmux kill-session" C-m 
tmux splitw -h
tmux send-keys "./launch-guest.sh" C-m 
tmux splitw -v
tmux send-keys "./launch-virtiofsd.sh" C-m 
tmux selectp -t 0
tmux splitw -v
tmux send-keys "./test.sh" C-m 
# tmux splitw -v
# tmux send-keys "bash -c 'until $SSH xen-domu echo alive; do sleep 1; done > /dev/null 2>&1; ./test.sh'" C-m 

tmux attach-session -t $session